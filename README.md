Git is a version control system.

Git helps you keep track of code changes.

Git is used to collaborate on code.

FEATURE BRANCH - A feature branch is a copy of the main codebase where an individual or team of software developers can work on a new 
feature until it is complete. The core idea behind the Feature Branch Workflow is that all feature development should take place in 
a dedicated branch instead of the main branch. This encapsulation makes it easy for multiple developers to work on a particular 
feature without disturbing the main codebase.

